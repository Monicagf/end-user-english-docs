#  Mailtrain

Mailtrain is an open source application that allows you to create email campaigns without having to use third party services, thus preserving the privacy of our community. 
It offers a very easy and intuitive interface for creating new lists, configuring subscription and shipping parameters, as well as many other useful options. All the instructions for use, with images, are included in the same tool.  
In this tutorial, we just give indications for the general configuration of the tool, so you can connect to the mail server and send the messages to the lists.  

## Change your password

In the installation process, Mailtrain creates the administrator account, assigning it as default password: 'test'.  
Unfortunately, when you access the application for the first time, there is no warning or reminder that invites you to change this password.  
The first thing you will have to do is replace the default password with one of your own.  

It is also advisable to assign a valid email account, associated with this account, which will allow you to receive notifications, or to recover the password in case of loss.
You can make both changes from the 'Account' page, which you can access from the drop-down menu on the top right.  

![Screenshot](img/mailtrain/Change-password.png)

## General setup  

On the 'Settings' page, enter the address under which your Mailtrain installation can be visited, i.e. the application's installation domain.  
You will also have to modify the Admin Email parameter so that system notifications can be sent correctly.  

![Screenshot](img/mailtrain/general-settings.png)  


Further down on the same page, you'll have to tell Mailtrain how to do the deliveries. Mailtrain does not include a mail server of its own, so you need to insert the necessary parameters to can use an existing mail server and a valid account.  
With MaadiX you have a default mail server, so we will link Mailtrain to this same server, using an email account that you have previously created.

![Screenshot](img/mailtrain/mail-server-settings.png)

- Select SMTP for sending.
- Hostname: the full name of your server
- Port: 465
- Encryption: Use TLS — usually selected for port 465
- Insert a valid email account and your password, to send the newsletters

You can check if your configuration is correct by clicking on the 'Check Mailer Config' button.  
