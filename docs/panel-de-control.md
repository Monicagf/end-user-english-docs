# Server registration


When the process of creating a virtual machine on MaadiX is complete, the same newly created server sends a notification email to the email account that has been designated as the administrator.  

The email contains instructions on how to access the server, including passwords. These passwords are generated locally by the server, so that no one but the server knows them.   

However, at least for the time being, the process of sending passwords by mail is done in plain text, which makes it absolutely essential to change them immediately for security reasons. This is why MaadiX forces the change of passwords through an activation process at the time of the first authentication in the control panel.  

# Activation 

The first time the control panel is accessed, only the activation page will be displayed. On this page you will have to change the password of two different system accounts:  

* The administration account for the control panel  
* The system root account (Superuser)    

Until this process is completed, the other sections of the control panel will not be accessible and the root account will not be able to access the system via SSH or SFTP. 

This is an important security measure, which also has to be applied to any other application that has been installed and requires a username and password. 

It is extremely important that the email associated to the control panel administration account is valid and that you have access to it, since the server will send to this account all the notifications, indications and instructions to recover the control panel access key in case you forget it.

# Details

The control panel startup window displays internal statistical information about resource usage on the system. It also shows information about the DNS settings for your server's main domain.   

![Screenshot](img/es/panel-de-control.png)
