# Server MySQL

Each virtual machine has its own MySQL server.

Credentials for the MySQL root user are sent in the service activation email. For security reasons, it is recommended to change them when the server is accessed for the first time.  

If you have chosen not to install the phpMyAdmin application, you can only manage the databases and their users through the console (terminal). Otherwise, this application can be used to manage the databases and their users.  


# phpMyAdmin

This is an application external to the control panel, which allows the administration of databases and MySQL users from a graphic interface.  

For security reasons, this application is protected with a double password. Only users with 'phpMyAdmin Application Access' activated will be able to access it.
You can activate this access when creating new users, or by editing the profile of existing users.  

![Screenshot](img/es/users/enable-phpmyadmin.png)  

## First password  

When the phpMyAdmin application is accessed, the first authentication is displayed with a pop-up window. In this first form you have to insert the user credentials and password of a user who has activated the service 'phpMyAdmin application access' (Warning: this is not the user and password of a MySQL user for a database or the MySQL root user -- that is the next step).  

![Phpmyadmin private area](img/private-area-phpmyadmin.png)

## Second password  

Once this authentication is successful, the interface of the phpMyAdmin application is displayed, which will request a MySQL user. By default there is a MySQL user whose name is 'root' and whose password is included in the email sent at the time of activation of the server.

![Phpmyadmin](img/phpmyadmin.png)

For security reasons, it is highly recommended that you change the password of the root user. You can do this from the application itself:

![Phpmyadmin change password ](img/phpmyadmin-chpswd.png)   


It is good practice to create a different MySQL user for each database, and thus grant permissions on only one and not on all the databases you have created.

Both databases and MySQL users and their passwords can be created and managed from phpMyadmin.  
By default, only the MySQL 'root' user has the necessary privileges to create new databases and new users, and to grant permissions to each of them.  

If you have any doubts or need further assistance, you can look up the official guidelines for the use of phpMyAdmin by [following this link.](https://www.phpmyadmin.net/docs/)
