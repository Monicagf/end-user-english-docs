# Create your website or application

## Choose your domain

In order to create a website accessible from any browser, you will need a domain or sub-domain. 

If you already have your own domain, MaadiX allows you to activate it and configure it easily for later use on your website and applications. If you have not yet activated your domain on MaadiX, you can find instructions on how to proceed in the [following tutorial](.md domains).

Alternatively, if you do not have your own domain, you can use your *subdomain.maadix.org*, where 'subdomain' matches the name you chose when you purchased your MaadiX server. You will have to create your web or application within the folder `/var/www/html/subdominio.maadix.org`, and you will be able to visit it from the browser in your *subdominio.maadix.org*.

The following tutorial describes the steps to follow if you have your own domain. However, the process is the same for using it in your *subdomain.maadix.org* if the previous information is taken into account.

## Upload content

Once you have [activated the domain](domains.md) for your new website, you will need to upload your application files to the folder located in `/var/www/html/mydomain.com'.   

You can access this location and easily upload files using an SFTP (Secure File Transfer Protocol) client. In case you don't have any SFTP client installed and don't know which one to choose, [Filezilla](https://filezilla-project.org/) is one of the most widely used and straightforward.

Naturally, the SFTP client will ask you for a set of credentials to connect to the server:

#### As a Webmaster user  

If when you activate your domain you have assigned it a specific Webmaster user (which we recommend), this will be the owner of the '/var/www/html/mydomain.com' folder and be the only one who can modify the files in it.

The credentials for the connection with the Webmaster user are:

* **Server**: your *subdomino.maadix.org*.
* **Protocol**: SFTP.
* **Access mode**: normal.
* **User**: the name of the Webmaster user (Warning: it is case sensitive)
* **Password**: the password you have set for this user.

![Screenshot](img/sftp-user.png)

When the connection is established, as a  Webmaster user you will see a folder with your name. In it, there will be a folder that gives access to `/var/www/html/middomain.com', or several if you are a Webmaster for other domains besides this one.

You can also have other files of your own that you have uploaded or created before. Webmaster users only have access to this area and not to all files on the system (as the Superuser does). 

<a id="domain-folder"></a>
#### Inside the folder `/var/www/html/midominio.com` 

Inside the folder `/var/www/html/mydomain.com` there are two elements: a folder called .well-known and an index.html file.  

The .well-known folder is required for a secure connection to the web (HTTPS) and should not be touched (*if you can't see the .well-known folder, don't worry. The dot in front of the name means that it is a hidden file, it is there but Filezilla, or your SFTP client are configured not to show hidden files*).  

![Screenshot](img/sftp-midominio.png)

The index.html file is a *placeholder* type file that is created in the domain activation process and is used to check that the domain activation has been successfully completed. If you visit your domain from a browser before uploading your own files, you will find the welcome page of this index.html file.  

[Welcome Screenshot]

You can now delete (or overwrite) this existing index.html file and **upload here the files of your website or application**. 

# As a System Superuser 

If you have not created a webmaster user for your new domain, the owner of the web folder at `/var/www/html/mydomain.com` will be your System Superuser.

You can establish connection by SFTP as your Superuser with the credentials:

* **Server**: your *subdominion.maadix.org*.
* **Protocol**: SFTP.
* **Access mode**: normal.
* **User**: the Superuser name (Warning: it is case sensitive).
* **Password**: the Superuser password.

If you don't remember your Superuser details, you can check them out in the 'Users' tab of your control panel -- it's the first one on the list. You can also reset your password there in case you have forgotten it.

The Superuser has high privileges and access to the entire system. When you establish the SFTP connection to the server as Superuser, the location you are in at login is your personal folder in `/home/superuser/`.

You will have to navigate through the folders until you reach `/var/www/html/middomain.com/`, which is where you will have to upload or create the files for your new website or application. Another faster option is to type the path `/var/www/html/mydomain.com/` in the "Remote Site" field.


Once there, you can follow the same steps described for the user [Webmaster](#domain-folder)

![Screenshot](img/sftp-midominio.png)

