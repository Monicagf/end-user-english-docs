# Categorías de usuarios

In MaadiX there are different categories of users that you can activate or manage on your system. Each one has different privileges and functions, which we will help you understand on this page so that you know  better know how to use them.


* **Control Panel Administrator**: this is the only account with unlimited data management privileges through the control panel. This account is automatically created at the same time as the server and cannot be deleted.
* **Superuser**: even though this account does not have access through the control panel, it is hierarchically more important than the previous one, since it has unlimited privileges over the whole system ("root user" of Linux). It cannot be removed, so you never lose root access to the system.
* **Ordinary users**: accounts that the administrator can create through the control panel, to which the administrator can grant permissions to use or to access the various services.    
* **Postmaster user**: for each domain activated in the control panel, a postmaster account is created. This type of account will have the possibility to create and manage email accounts associated with the same domain. 
* **Email accounts**: email accounts will have access to the control panel with the only privilege of viewing or editing their own data (password, name, automatic forwarding...).  

## Administrator

The control panel administrator is probably the account you will use the most, since it has all the privileges within the control panel. This account can create, configure and delete all other accounts as well as domains, email accounts and any other data that can be managed from the control panel.

This account is created automatically and cannot be deleted. Otherwise, you would lose access to the control panel.   
When we build the server from MaadiX, we also create this account, assigning it a random password that for security reasons you will have to change the first time you access the control panel.

All parameters, except the username, can be edited at any time from the 'Profile' page of the control panel. You can access this page through the menu in the upper right hand corner.    

![admin](../img/es/users/edit-profile.png)

It is very important that the email associated with this account is valid and that you have access to it, since the system uses it to send certain notifications or for important functions, such as password recovery.


  
## Superuser

This account cannot be traded from the control panel (in fact, it does not even have valid access to it). Still, it is the most powerful account in the system. Technically speaking, it is the 'root' user.  
Having 'root' access to a system means having full control over it, being able to implement any kind of change, access any folder and install any applications from the console.  

 
For security reasons, it is advisable to limit the use of this account whenever possible, creating ordinary user accounts instead. More details on how to create and use these type of accounts can be found in this document.  

 
Like the administrator, the Superuser is automatically created in the server creation process, and until you change its password during the control panel activation process, it will be an inactive account, without any access.  

Once activated, this account will be able to access the server through an SFTP or SSH connection. Its privileges in the system are unlimited, so it allows you to perform any of the following operations, which are not allowed from the control panel:

* Access all folders in the system through one connection (STFP/SSH).
* Read, modify, delete or execute any system file even if it is not proprietary (SSH).
* Modify, install or remove any application (SSH).
* Create other`root`accounts.
* Shut down or restart the server.


## Ordinary users

These are accounts created by the administrator and to which different degrees of permissions and access can be assigned.

* **Access SFTP**: will be able to access the server via SFTP and will be confined to their own personal folder. They cannot access the rest of the system, nor do they have SSH access.
* **Webmaster**: can be appointed as webmasters of a given web application. In this case, a shortcut is created in your personal folder to the web application folder. It becomes the owner of this folder and all the files it may contain, acquiring all the privileges over these files. Note that webmaster accounts will need to have SFTP access activated in order to edit the files.   
* **Account VPN**: a VPN account can be activated so that the connection to the server is direct and secure. They can also use this connection to connect to any other Internet address.
* **Access to phpMyAdmin application**: you can activate permission to access this application, if it is installed. For security reasons, this application is protected by a double password, and access to the interface must be enabled in order to gain access.

## Postmaster users

This account only has the power to manage the email accounts associated with its domain. It can create, modify and delete e-mail addresses through the control panel. This type of account is extremely useful to allow these tasks without having to give someone else all the privileges.  

Postmaster accounts are created by default for each domain that is enabled in the control panel, with a random password that can be edited later.


To operate from the control panel, postmaster accounts will have to identify themselves using the username postmaster@yourdomain.com and the password assigned to them.  

![Postmaster log in](../img/es/users/postmaster-login.png)

  
Once inside the control panel, postmaster accounts will have a slightly different interface than the administrator's. They will only have access to the editing features of the email accounts associated with their domain, as well as editing their own profile.

![Postmaster logged in](../img/es/users/postamster-logged.png)


## Email accounts

Email accounts created in the control panel can be managed directly by their owners.  
The person who has an activated email will be able to enter the control panel inserting his or her email account as user name and the password assigned to it.     

![Email log in](../img/es/users/email-login.png)  

Editing privileges are limited to the user's own email account. He or she will be able to change the password, first and last name, as well as set or disable forwarding and auto-reply features.   
The email account user can also access the data for the configuration of the account in an email client on his or her own device (Thunderbird, Outlook...)   

![Email logged in](../img/es/users/email-logged.png)  


