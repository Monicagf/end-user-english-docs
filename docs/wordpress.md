# Install WordPress

1. Download the application from the  [official website](https://wordpress.org/download/).

![Screenshot](img/wpimages/download-wordpress.png)

Unzip the folder.

2. Upload to the server the files in the "wordpress" folder, inside the domain folder that was generated when the domain was activated from the control panel. If you have added the domain example.com, the folder will be `/var/www/html/example.com/`.

To do this, you can use an SFTP client such as Filezilla.
Warning: You should not upload the "wordpress" folder as such, only the files it contains.

3. Create the database

To create a database, you can use the phpMyAdmin application.

## Importing an existing database

If you already have a MySQL database of your WordPress (for example, in cases of migration from another server), it is necessary to import it. From phpMyAdmin, you have to select the "Import" section and upload the corresponding file (.sql or .gzip, .bzip2 or .zip if it is compressed).

![Screenshot](img/wpimages/importar-bdd.png)

Once imported, we have to select the "Privileges" section to create a new user that can access, read and write to the database (it is not advisable to use the same root user for security reasons).


Choose a name and password and click "Grant all database privileges".
Write down the username and password of the MySQL user you just created, since they will be needed during the WordPress installation process.

![Screenshot](img/wpimages/otorgar-privilegios.png)

## Creating a new database

If this is a new installation and we don't have a MySQL database created yet, we need to create one. Inside phpMyAdmin, we have to go to "Databases" > "Create database".

![Screenshot](img/wpimages/crear-bdd.png)

Once created, we have to select the "Privileges" section to create a new user that can access, read and write to the database (it is not advisable to use the same root user for security reasons).


Choose a name and password and click "Grant all database privileges".
Write down the username and password of the MySQL user you just created, since they will be needed during the WordPress installation process.

![Screenshot](img/wpimages/otorgar-privilegios.png)

## Configuring WordPress

When we have created the database, we have to visit the domain with the browser. A form will appear to finish the installation process of WordPress, in which the credentials of the MySQL user we have created will be requested.

![Screenshot](img/wpimages/formulario-wordpress2.png)

Remember that changing the wp_ prefix to another in the "Table Prefix" section adds an extra layer of security to your installation.

# Common problems with WordPress installation

## I can't install plugins or upload images

Make sure the permissions of the files you have uploaded via SFTP are correct. For WordPress to be able to upload images and plugins from the admin panel, the /var/www/html/example.com/wp-content folder must have the following permissions:

`drwxrwsr-x`

To check that the permissions are correct, from FileZilla you must right-click on the wp-content folder and select the option "File permissions" or "File attributes", depending on your version.

![Screenshot](img/herramientas-seleccion.png)

The permissions have to stay this way, being the numerical value 775. You also have to check the option "Activate all files and directories".

![Screenshot](img/wpimages/permisos.png)

--

Even if the permissions of the WordPress folders are correct, you may have problems installing plugins or uploading images. To fix this, add the following line to the end of the wp-config.php file

  `define( 'FS_METHOD', 'direct' );`.

When you upload images or Plugins from WordPress, rather than from an SFTP client, it is not your own user who executes the operation, but the Apache user (www-data).
Although this user has write permissions on the files, he or she is not the owner.
On shared servers, this could represent a security problem, since the same www-data user could be used by everyone who has access.
Therefore, WordPress uses a method in which it checks (in addition to permissions) whether the owner of the files matches the user who is performing the write operation -- a situation that does not occur, so it does not proceed with the operation and asks for your FTP credentials.

On MaadiX you have no FTP or FTPs, only SFTP , which uses port 22 instead of 21, which is the one WordPress uses without giving you the option to change it.
The solution of defining the method as 'direct' makes WordPress directly access the file system, where the www-data user has the necessary permissions to write.

If you want to know more, you can check these two links:

https://wordpress.stackexchange.com/questions/189554/what-security-concerns-should-i-have-when-setting-fs-method-to-direct-in-wp-co
https://wordpress.stackexchange.com/a/232291
